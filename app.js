var express = require('express');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');

var app = express();

/* On utilise les cookies, les sessions et les formulaires */
app.use(cookieParser())
.use(session({secret: 'todotopsecret'}))
.use(bodyParser.urlencoded({ extended: false }))

.use(function(req, res, next){
    if (typeof(req.session.todos) == 'undefined') {
        req.session.todos = [];
    }
    next();
})

.get('/todo', function(req, res) {
	var session = req.session;
    res.render('todolist.ejs', {todos: session.todos});
})
.get('/todo/delete/:id', function(req, res) {
	if (req.params.id != '') {
    	req.session.todos.splice(req.params.id, 1);
    }
    res.redirect('/todo');
})
.post('/todo/add', function(req, res) {
   req.session.todos.push(req.body.todo_text);
   res.redirect('/todo');
})
.use(function(req, res, next){
    res.redirect('/todo');
});

app.listen(8080);